// Number of matches won per team per year in IPL.
const csv = require("csv-parser");
const fs = require("fs");
const path = require('path')

const filepath=path.join(__dirname,"../data/matches.csv")
const anotherFilePath = path.join(__dirname, "../public/output/TeamMatchesWonPerYear2.js" )

const results = [];
let finalAns = {};
function TeamsWonPerYear() {
  fs.createReadStream(filepath)
    .pipe(csv())
    .on("data", (data) => results.push(data))
    .on("end", () => {
      
      for (let matches of results) {
        let season = matches["season"];
        let winner = matches["winner"].trim();
        // console.log(season , winner)
        if (finalAns.hasOwnProperty(season) == false) { // I am checking whether my finalAns has season(year or not)
          finalAns[season] = {};
        }
        if (finalAns[season].hasOwnProperty(winner) === false) { /// cehcking the team is there in the particular year or not
          finalAns[season][winner] = 1; // if not add 1 to it.
        } else { 
          finalAns[season][winner] += 1; // if already present. add extra 1 to it.
        }
      }
      // console.log(finalAns);
     
 
      fs.writeFile(anotherFilePath, JSON.stringify(finalAns, null, 2), (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Results have been exported to", anotherFilePath);
        }
      });
      // console.log(finalAns)
      // return finalAns
      
    });
}

module.exports = TeamsWonPerYear;
