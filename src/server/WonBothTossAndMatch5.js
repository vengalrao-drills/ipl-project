// find the number of times each team won the toss and also won the match

const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

const matches = [];
const filepathMatches = path.join(__dirname, "../data/matches.csv");
const outputFilePath = path.join(__dirname, "../public/output/WonBothTossAndMatch5.js");
function WonBothTossAndMatch() {
  fs.createReadStream(filepathMatches)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      // console.log(matches[0])
      let finalAns = {};

      for (let match of matches) {
        let toss = match["toss_winner"];
        let winner = match["winner"];
        // console.log(toss, winner)

        if (toss.toLowerCase() == winner.toLowerCase()) {  //Checking comparing both the toss and winner
          if (finalAns.hasOwnProperty(toss) == false) {
            finalAns[toss] = Number(1);
          } else {
            finalAns[toss] += Number(1);
          }
        }
      }

      // console.log(finalAns);

      fs.writeFile(outputFilePath, JSON.stringify(finalAns, null, 2), (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Results have been exported to", outputFilePath);
        }
      });
    });
}
module.exports = WonBothTossAndMatch;
