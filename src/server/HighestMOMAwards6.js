// find a player who has won the highest number of Player of the Match awards for each season

const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

const matches = [];
const filepathMatches = path.join(__dirname, "../data/matches.csv");
const outputFilePath = path.join(
  __dirname,
  "../public/output/HighestMOMAwards6.js"
);
function HighestMOMAwards() {
  fs.createReadStream(filepathMatches)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      let totalYears = {};
      // Here I am checking the start index and the end index of the season. The data in the matches is sorted
      for (let match of matches) {
        let id = match.id;
        let season = match.season;
        if (totalYears.hasOwnProperty(season) == false) {
          totalYears[season] = [id, id];
        } else {
          totalYears[season][1] = id;
        }
      }
      // console.log(totalYears)
      //Till here I got the total years and I have start and the end index
      // In the format of [start , end]

      function seasonAwards(start, end) {
        start -= 1;
        end -= 1;
        let personAwards = {};
        let highestMOM = ["", -1];
        for (let index = start; index <= end; index++) {
          let manOfTheMatch = matches[index]["player_of_match"]; //  man of the match is the player
          if (personAwards.hasOwnProperty(manOfTheMatch) == false) {
            //Taking whether the person has record in personalAward
            personAwards[manOfTheMatch] = 1;
            if (highestMOM[1] < personAwards[manOfTheMatch]) {
              //Created separate final highest Man-of-the-Match. And now comparing with to the individual record
              highestMOM[0] = manOfTheMatch;
              highestMOM[1] = personAwards[manOfTheMatch];
            }
          } else {
            personAwards[manOfTheMatch] += 1;

            if (highestMOM[1] < personAwards[manOfTheMatch]) {
              highestMOM[0] = manOfTheMatch;
              highestMOM[1] = personAwards[manOfTheMatch];
            }
          }
        }
        return highestMOM;
      }
      // console.log(seasonAwards(Number(60), Number(117)))
      let finalAns = {};
      for (let years in totalYears) {
        // In total years I have the start and the ending value now I will passed it to the function season awards
        let [playerName, totalCount] = seasonAwards( totalYears[years][0],totalYears[years][1]);
        // console.log(playerName, totalCount);
        // if(finalAns[years].hasOwnProperty(playerName)==false){
        //     finalAns[years][playerName] = totalCount
        // }

        finalAns[years] = [playerName, totalCount];
      }
      //   console.log(finalAns);

      // console.log(matches[0])

      fs.writeFile(outputFilePath, JSON.stringify(finalAns, null, 2), (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Results have been exported to", outputFilePath);
        }
      });
    });
}
module.exports = HighestMOMAwards;
