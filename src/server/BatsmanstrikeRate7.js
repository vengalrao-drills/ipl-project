// find the strike rate of a batsman for each season

const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");
let matches = [];
let deliveries = [];

const filepathMatches = path.join(__dirname, "../data/matches.csv");
const filepathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath = path.join(__dirname, "../public/output/BatsmanstrikeRate7.js");
function BatsmanStrikeRate() {
  fs.createReadStream(filepathMatches)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      let totalYears = {};
      for (let match of matches) {
        let season = match.season;
        let id = match.id;
        if (totalYears.hasOwnProperty(season) == false) { // finding index(start , end)
          totalYears[season] = [id, id]; // [start , end]
        } else {
          totalYears[season][1] = id; 
        }
      }
      // console.log(totalYears)

      fs.createReadStream(filepathDeliveries)
        .pipe(csv())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {
          // console.log(deliveries[0])

          let finalAns = {}; // THIS IS MY FINAL OBJECT. which my ans is going to be stored

          let onlyMatchId = [];
          for (let index = 0; index < deliveries.length; index++) {
            onlyMatchId.push(Number(deliveries[index].match_id));
          }
          // let l = onlyMatchId
          // console.log(onlyMatchId)

          function StrikeRate(deliveries, start, end) {
            let myStart = onlyMatchId.indexOf(start);
            let myEnd = onlyMatchId.lastIndexOf(end);
            // console.log(start , end ,myStart , myEnd)
            let strike = {};
            for (let index = myStart; index < myEnd; index++) {
              let batsman = deliveries[index].batsman; 
              let runByBatsman = Number(deliveries[index].batsman_runs);

              if (strike.hasOwnProperty(batsman) == false) {  // hecking whether the batsman has peoperty in {strike - object} or not !
                strike[batsman] = [runByBatsman, 1]; // First strike we need both runs and the number of balls he faced
              } else {
                strike[batsman][0] += runByBatsman; // runs adding
                strike[batsman][1] += 1;  // number of balls 
              }
            }
            finalStrike = {};
            for (let index in strike) {
              let runs = strike[index][0];
              let balls = strike[index][1];
              finalStrike[index] = Number((runs / balls) * 100).toFixed(2); //Adding the finalvalue (strike rate) to the finalStrike(array)
            }
            return finalStrike;
          }
          // console.log(StrikeRate(deliveries   , Number(60) , Number(117) ))
          // let (indexof )

          for (let years in totalYears) {
            let start = totalYears[years][0];
            let end = totalYears[years][1];
            finalAns[years] = StrikeRate( deliveries,  Number(start),  Number(end) );
            // Iterating through all the years and getting all the batsman strike rate
          }
          // console.log(finalAns)

          fs.writeFile(
            outputFilePath,
            JSON.stringify(finalAns, null, 2),
            (err) => {
              if (err) {
                console.error("Error writing file:", err);
              } else {
                console.log("Results have been exported to", outputFilePath);
              }
            }
          );
        });
    });
}
module.exports = BatsmanStrikeRate;
