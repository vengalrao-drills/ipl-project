// Find the bowler with the best economy in super overs
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path")


const filepathDeliveries = path.join(__dirname, "../data/deliveries.csv"); 
const outputFilePath = path.join(__dirname, "../public/output/superOvers9.js");
const deliveries = [];
function superOvers() {
  fs.createReadStream(filepathDeliveries)
    .pipe(csv())
    .on("data", (data) => deliveries.push(data))
    .on("end", () => {
      // console.log(deliveries[0]);

      let Objectbowlers = {};
      for (let ball of deliveries) {
        let bowler = ball.bowler;
        let totalRuns = Number(ball.total_runs);
        let superOver = Number(ball.is_super_over);
        if (superOver > 0) {
          if (Objectbowlers.hasOwnProperty(bowler) == false) {
            Objectbowlers[bowler] = [Number(totalRuns), 1]; //Here in super over need to check count both the number of runs and balls
          } else {
            Objectbowlers[bowler][0] += Number(totalRuns);
            Objectbowlers[bowler][1] += 1;
          }
        }
      }
      // console.log(Objectbowlers)

      let finalAns = {};
      for (let bowlers in Objectbowlers) {
        let index1 = Objectbowlers[bowlers][0];
        let index2 = Objectbowlers[bowlers][1];
        finalAns[bowlers] = (index1 * 6) / index2;
      }
      // console.log(finalAns)
      let entriesArray = Object.entries(finalAns);

      let topSort = entriesArray.sort((player1, player2) => {
        return player1[1] - player2[1]; //  Sorting according to the economy rate
      });
      console.log();
      let finaAnsSentence = `SuperOver best economy bowler is ${topSort[0][0]}\nhis economy ${topSort[0][1]} `
      // console.log(finaAnsSentence)

      // for (let i = 0; i <= 10; i++) {
      //   console.log(topSort[i]);
      // }

      fs.writeFile(outputFilePath, JSON.stringify(finaAnsSentence, null, 2), (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Results have been exported to", outputFilePath);
        }
      });
    });
} 
module.exports = superOvers;
