// Number of matches played per year for all the years in IPL.
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

const filepath = path.join(__dirname, "../data/matches.csv");
const anotherFilePath = path.join(
  __dirname,
  "../public/output/MatchesPerYear1.js"
);
// console.log(path.dirname(filepath)+ '/data/matches.csv'  )
const matches = [];
function NumberOfMatchesPlayed() {
  fs.createReadStream(filepath)
    .pipe(csv())
    .on("data", (data) => matches.push(data))  //Now I have loaded all my matches (data) from matches.csv file
    .on("end", () => {
      let finalAns =  {}
      // console.log(matches[0])
      for (let match of matches){
        let season = match.season;
        if(finalAns.hasOwnProperty(season)==false){  //I am checking weather the season(year) is present in the finalObject or not
          finalAns[season] = 1
        }else{
          finalAns[season]+=1  // If present I will add
        }
      }

        // console.log(finalAns); 

        // Now Writing to finalAns (Final) data another file
      fs.writeFile(
        anotherFilePath,
        JSON.stringify(finalAns, null, 2),
        (err) => {
          if (err) {
            console.error("Error writing file:", err);
          } else {
            console.log("Results have been exported to", anotherFilePath);
          }
        }
      ); 
    });
}

module.exports = NumberOfMatchesPlayed;
