// Top 10 economical bowlers in the year 2015
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

const matches = [];
let deliveries = [];
let finalAnsOver = [];

const filepathMatches = path.join(__dirname, "../data/matches.csv");
const filepathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const anotherFilePath = path.join( __dirname,  "../public/output/TopEconomicalBowlers4.js");

function TopEconomicalBowlers() {
  fs.createReadStream(filepathMatches)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => {
      let start = -1;
      let flag = -1;
      let end = -1;

      for (let match of matches) {
        // finding the start and ending index season - 2015
        if (flag == -1 && match["season"] == 2015) {
          start = Number(matches.indexOf(match)) + 1;
          flag = 0;
        } else if (match["season"] == 2015) {
          end = Number(matches.indexOf(match)) + 1;
        }
      }
      // console.log(start, end);

      fs.createReadStream(filepathDeliveries)
        .pipe(csv())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {
          let Objectbowlers = {};
          for (let ball of deliveries) {
            let bowler = ball.bowler;
            let totalRuns = ball.total_runs;
            let matchId = ball.match_id;
            if (matchId <= end && matchId >= start) {
              if (Objectbowlers.hasOwnProperty(bowler) == false) {// Here I am checking whether the bowler is not htere in objectBowlers
                Objectbowlers[bowler] = [Number(totalRuns), 1];  //It's not present now I am creating a object (bowler) for it. And assigning  a array  to it
              } else {
                //every time add totalnumber of runs. and also the number of bowls.
                // to count the overs
                Objectbowlers[bowler][0] += Number(totalRuns);
                Objectbowlers[bowler][1] += 1;
              }
            }
          }
          // console.log(Objectbowlers);
          let finalAns = {};
          for (let bowlers in Objectbowlers) {
            // console.log(Objectbowlers[bowlers][0])
            let index1 = Objectbowlers[bowlers][0];
            let index2 = Objectbowlers[bowlers][1];
            finalAns[bowlers] = (index1 * 6) / index2;  // converted to the economy rate
          }
          let entriesArray = Object.entries(finalAns); // to sort the array. we need an arrya

          //Data is in the format of [bowlername , economy] 
          let topSort = entriesArray.sort((player1, player2) => {
            // Now I need to sort according to the 1 index
            return player1[1] - player2[1];
          });
          for (let i = 0; i <= 10; i++) {
            finalAnsOver.push(topSort[i]);
          }

          // console.log(finalAns)
 
          fs.writeFile(anotherFilePath, JSON.stringify( finalAnsOver , null, 2), (err) => {
            if (err) {
              console.error("Error writing file:", err);
            } else {
              console.log("Results have been exported to", anotherFilePath);
            }
          });
        });
    });
}

module.exports = TopEconomicalBowlers;
