// find the highest number of times one player has been dismissed by another player

const csv = require("csv-parser");
const fs = require("fs");
const path = require("path")

const deliveries = []; 

const filepathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath = path.join(__dirname,"../public/output/dismissals8.js");

function dismissals() {
  fs.createReadStream(filepathDeliveries)
    .pipe(csv())
    .on("data", (data) => deliveries.push(data))
    .on("end", () => { 
      // console.log(deliveries[0]);

      let batsmanHistory = {};

      for (let ball of deliveries) {
        let bowler = ball["bowler"];
        let dismissedPLayer = ball["player_dismissed"];
        let batsman = ball["batsman"];
        

        if (batsmanHistory.hasOwnProperty(batsman) == false) { 
          batsmanHistory[batsman] = {};
        }
        if (dismissedPLayer.length != "") {
          if (batsmanHistory[batsman].hasOwnProperty(bowler) == false) {
            batsmanHistory[batsman][bowler] = 1;
          } else {
            batsmanHistory[batsman][bowler] += 1;
          }
        }
      }
      // console.log(batsmanHistory);
      // data stored in 'batsmanHistory' is { bowler : { batsman : count dismissals ......  }  }

      function maxDismissed(name, value) {
        // let maximum = ['' , -1 ];
        // for(let players in value){
        //   if( value[players] > maximum[1] ){
        //     maximum[1]=  value[players]
        //     maximum[0] = players
        //   }
        // }
        // return maximum
        let maximum = { Bowler: "", TimesDissmissed: -1 };

        for (let player in value) {
          if (value[player] > maximum["TimesDissmissed"]) {
            maximum["Bowler"] = player;
            maximum["TimesDissmissed"] = value[player];
          }
        }

        return maximum;
      }
      let finalAns = {};
      for (let batters in batsmanHistory) {
        let c = maxDismissed(batters, batsmanHistory[batters]);
        if (c["Bowler"].length > 0) {
          // here in my data there is some null values. that they have not even taken a single wicktet.
          
          finalAns[batters] = maxDismissed(batters, batsmanHistory[batters]);
        }
      }
      // console.log(finalAns);


      fs.writeFile(outputFilePath, JSON.stringify(finalAns, null, 2), (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Results have been exported to", outputFilePath);
        }
      });
    });
}
module.exports = dismissals;


















// for (let ball of deliveries) {
//   let bowler = ball["bowler"];
//   let dismissedPLayer = ball["player_dismissed"];
//   let batsman = ball['batsman']

//   if (bowlersHistory.hasOwnProperty(batsman) == false) {
//     bowlersHistory[batsman] = {};
//   }
//   if (  dismissedPLayer.length != '') {
//     if (bowlersHistory[batsman].hasOwnProperty(bowler) == false) {
//       bowlersHistory[batsman][bowler] = 1;
//     } else {
//       bowlersHistory[batsman][bowler] += 1;
//     }
//   }
// }
// console.log(bowlersHistory);

// for (let ball of deliveries) {
//   let bowler = ball["bowler"];
//   let dismissedPLayer = ball["player_dismissed"];

//   if (bowlersHistory.hasOwnProperty(bowler) == false) {
//     bowlersHistory[bowler] = {};
//   }
//   if (  dismissedPLayer.length !=  '') {
//     if (bowlersHistory[bowler].hasOwnProperty(dismissedPLayer) == false) {
//       bowlersHistory[bowler][dismissedPLayer] = 1;
//     } else {
//       bowlersHistory[bowler][dismissedPLayer] += 1;
//     }
//   }
// }
// console.log(bowlersHistory);
