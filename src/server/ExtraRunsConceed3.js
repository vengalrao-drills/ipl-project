// Extra runs conceded per team in the year 2016
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

const filepathMatches = path.join(__dirname, "../data/matches.csv");
const filepathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const anotherFilePath = path.join( __dirname,  "../public/output/ExtraRunsConceed3.js");

const matches = [];
let deliveries = [];
let finalAns2016 = {};  //This is the variable where my final answer is going to be store

function ExtraRunsConceed2016() {
  fs.createReadStream(filepathMatches)
    .pipe(csv())
    .on("data", (data) => matches.push(data))
    .on("end", () => { 
      fs.createReadStream(filepathDeliveries)
        .pipe(csv())
        .on("data", (data) => deliveries.push(data))
        .on("end", () => {
            // The data is sorted manner in Matches.
            // So i am finding the data which 2016 index starts and where it ends.
          let start = -1;
          let flag = -1;
          let end = -1;
        //   console.log( matches[0])
          for (let match of matches) {
            if (flag == -1 && match["season"] == 2016) {
              start = Number(matches.indexOf(match)) + 1;
              flag = 0;
            } else if (match["season"] == 2016) {
              end = Number(matches.indexOf(match)) + 1;
            }
          }
        //   console.log(start , end)
        // Till here I have found the  start and end  index value of 2016

          for (let ball of deliveries) {
            let match_id = Number(ball.match_id); 
            let bowlingTeam = ball.batting_team;
            let extraRuns = ball.extra_runs;

            if (match_id >= start && match_id <= end) {
              if (finalAns2016.hasOwnProperty(bowlingTeam) == false) { //If final answer 2016 doesn't have the season  then will be going to add Team to it
                finalAns2016[bowlingTeam] = Number(extraRuns);
              } else {
                finalAns2016[bowlingTeam] += Number(extraRuns);   //If present I will be directly adding the extra runs
              }
            }
          }

        //   console.log(finalAns2016) 
          fs.writeFile(
            anotherFilePath,
            JSON.stringify(finalAns2016, null, 2),
            (err) => {
              if (err) {
                console.error("Error writing file:", err);
              } else {
                console.log("Results have been exported to", anotherFilePath);
              }
            }
          );
        //   return finalAns2016
        });
    });
}
module.exports = ExtraRunsConceed2016;
