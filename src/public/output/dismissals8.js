{
  "DA Warner": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "S Dhawan": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 4
  },
  "MC Henriques": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 2
  },
  "Yuvraj Singh": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 3
  },
  "DJ Hooda": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 2
  },
  "BCJ Cutting": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 1
  },
  "CH Gayle": {
    "Bowler": "Sandeep Sharma",
    "TimesDissmissed": 4
  },
  "Mandeep Singh": {
    "Bowler": "VR Aaron",
    "TimesDissmissed": 2
  },
  "TM Head": {
    "Bowler": "Rashid Khan",
    "TimesDissmissed": 1
  },
  "KM Jadhav": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 2
  },
  "SR Watson": {
    "Bowler": "AR Patel",
    "TimesDissmissed": 5
  },
  "Sachin Baby": {
    "Bowler": "Bipul Sharma",
    "TimesDissmissed": 1
  },
  "STR Binny": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 3
  },
  "S Aravind": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 1
  },
  "YS Chahal": {
    "Bowler": "C de Grandhomme",
    "TimesDissmissed": 1
  },
  "TS Mills": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 1
  },
  "A Choudhary": {
    "Bowler": "BCJ Cutting",
    "TimesDissmissed": 1
  },
  "PA Patel": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 6
  },
  "JC Buttler": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 2
  },
  "RG Sharma": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 6
  },
  "N Rana": {
    "Bowler": "A Zampa",
    "TimesDissmissed": 1
  },
  "AT Rayudu": {
    "Bowler": "MM Sharma",
    "TimesDissmissed": 5
  },
  "KH Pandya": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 1
  },
  "KA Pollard": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 3
  },
  "HH Pandya": {
    "Bowler": "Mustafizur Rahman",
    "TimesDissmissed": 2
  },
  "TG Southee": {
    "Bowler": "RA Jadeja",
    "TimesDissmissed": 1
  },
  "AM Rahane": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 6
  },
  "MA Agarwal": {
    "Bowler": "R Ashwin",
    "TimesDissmissed": 3
  },
  "SPD Smith": {
    "Bowler": "RA Jadeja",
    "TimesDissmissed": 4
  },
  "BA Stokes": {
    "Bowler": "HH Pandya",
    "TimesDissmissed": 1
  },
  "MS Dhoni": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 7
  },
  "JJ Roy": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 1
  },
  "BB McCullum": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 4
  },
  "SK Raina": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 5
  },
  "AJ Finch": {
    "Bowler": "P Negi",
    "TimesDissmissed": 3
  },
  "KD Karthik": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 4
  },
  "G Gambhir": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 6
  },
  "CA Lynn": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 2
  },
  "MK Tiwary": {
    "Bowler": "M Morkel",
    "TimesDissmissed": 4
  },
  "DT Christian": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 2
  },
  "HM Amla": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 2
  },
  "M Vohra": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 2
  },
  "WP Saha": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 3
  },
  "AR Patel": {
    "Bowler": "SP Narine",
    "TimesDissmissed": 3
  },
  "GJ Maxwell": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 4
  },
  "DA Miller": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 3
  },
  "Vishnu Vinod": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "Iqbal Abdulla": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 1
  },
  "P Negi": {
    "Bowler": "CH Morris",
    "TimesDissmissed": 2
  },
  "AP Tare": {
    "Bowler": "KK Cooper",
    "TimesDissmissed": 2
  },
  "SW Billings": {
    "Bowler": "Iqbal Abdulla",
    "TimesDissmissed": 1
  },
  "KK Nair": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 4
  },
  "SV Samson": {
    "Bowler": "MJ McClenaghan",
    "TimesDissmissed": 4
  },
  "RR Pant": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 3
  },
  "CH Morris": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 2
  },
  "CR Brathwaite": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 1
  },
  "PJ Cummins": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 1
  },
  "A Mishra": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 2
  },
  "S Nadeem": {
    "Bowler": "SP Narine",
    "TimesDissmissed": 3
  },
  "Z Khan": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 2
  },
  "DR Smith": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 4
  },
  "DS Kulkarni": {
    "Bowler": "CJ Jordan",
    "TimesDissmissed": 2
  },
  "P Kumar": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 3
  },
  "Basil Thampi": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "RV Uthappa": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 5
  },
  "MK Pandey": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 4
  },
  "YK Pathan": {
    "Bowler": "MM Sharma",
    "TimesDissmissed": 4
  },
  "SA Yadav": {
    "Bowler": "JP Faulkner",
    "TimesDissmissed": 2
  },
  "CR Woakes": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "SP Narine": {
    "Bowler": "MG Johnson",
    "TimesDissmissed": 2
  },
  "Harbhajan Singh": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 4
  },
  "AB de Villiers": {
    "Bowler": "KH Pandya",
    "TimesDissmissed": 4
  },
  "CJ Anderson": {
    "Bowler": "SP Narine",
    "TimesDissmissed": 2
  },
  "F du Plessis": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "RA Tripathi": {
    "Bowler": "P Negi",
    "TimesDissmissed": 2
  },
  "R Bhatia": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 2
  },
  "DL Chahar": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "A Zampa": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 1
  },
  "AB Dinda": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 2
  },
  "Imran Tahir": {
    "Bowler": "STR Binny",
    "TimesDissmissed": 1
  },
  "NV Ojha": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 4
  },
  "V Shankar": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "Rashid Khan": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 1
  },
  "B Kumar": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 2
  },
  "MP Stoinis": {
    "Bowler": "SP Narine",
    "TimesDissmissed": 1
  },
  "MM Sharma": {
    "Bowler": "CR Woakes",
    "TimesDissmissed": 1
  },
  "VR Aaron": {
    "Bowler": "CR Woakes",
    "TimesDissmissed": 1
  },
  "V Kohli": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 6
  },
  "MJ McClenaghan": {
    "Bowler": "S Badree",
    "TimesDissmissed": 1
  },
  "Ankit Sharma": {
    "Bowler": "AJ Tye",
    "TimesDissmissed": 1
  },
  "SN Thakur": {
    "Bowler": "AJ Tye",
    "TimesDissmissed": 1
  },
  "C de Grandhomme": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 1
  },
  "Bipul Sharma": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "SS Iyer": {
    "Bowler": "Sandeep Sharma",
    "TimesDissmissed": 3
  },
  "EJG Morgan": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 2
  },
  "KC Cariappa": {
    "Bowler": "CH Morris",
    "TimesDissmissed": 1
  },
  "Sandeep Sharma": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 1
  },
  "Ishan Kishan": {
    "Bowler": "Imran Tahir",
    "TimesDissmissed": 2
  },
  "JD Unadkat": {
    "Bowler": "MG Johnson",
    "TimesDissmissed": 1
  },
  "AF Milne": {
    "Bowler": "BA Stokes",
    "TimesDissmissed": 1
  },
  "S Badree": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "AD Mathews": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 3
  },
  "Mohammed Shami": {
    "Bowler": "VR Aaron",
    "TimesDissmissed": 1
  },
  "Mohammad Nabi": {
    "Bowler": "Sandeep Sharma",
    "TimesDissmissed": 1
  },
  "I Sharma": {
    "Bowler": "S Kaul",
    "TimesDissmissed": 1
  },
  "RA Jadeja": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 4
  },
  "AJ Tye": {
    "Bowler": "Sandeep Sharma",
    "TimesDissmissed": 1
  },
  "KS Williamson": {
    "Bowler": "CH Morris",
    "TimesDissmissed": 2
  },
  "SE Marsh": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 3
  },
  "Shakib Al Hasan": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 3
  },
  "JP Faulkner": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 2
  },
  "MG Johnson": {
    "Bowler": "JP Faulkner",
    "TimesDissmissed": 3
  },
  "K Rabada": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 1
  },
  "AD Nath": {
    "Bowler": "KC Cariappa",
    "TimesDissmissed": 1
  },
  "NM Coulter-Nile": {
    "Bowler": "P Negi",
    "TimesDissmissed": 1
  },
  "Kuldeep Yadav": {
    "Bowler": "S Aravind",
    "TimesDissmissed": 1
  },
  "UT Yadav": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 2
  },
  "KV Sharma": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 2
  },
  "SP Jackson": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 1
  },
  "MJ Guptill": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 2
  },
  "Anureet Singh": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 1
  },
  "IK Pathan": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 3
  },
  "Ankit Soni": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "JJ Bumrah": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "SL Malinga": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 2
  },
  "PJ Sangwan": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "LMP Simmons": {
    "Bowler": "R Ashwin",
    "TimesDissmissed": 2
  },
  "MN Samuels": {
    "Bowler": "KV Sharma",
    "TimesDissmissed": 1
  },
  "Swapnil Singh": {
    "Bowler": "CR Woakes",
    "TimesDissmissed": 1
  },
  "R Tewatia": {
    "Bowler": "SN Thakur",
    "TimesDissmissed": 1
  },
  "MM Patel": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 1
  },
  "SS Tiwary": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 2
  },
  "CJ Jordan": {
    "Bowler": "NM Coulter-Nile",
    "TimesDissmissed": 1
  },
  "IR Jaggi": {
    "Bowler": "KV Sharma",
    "TimesDissmissed": 1
  },
  "PP Chawla": {
    "Bowler": "NLTC Perera",
    "TimesDissmissed": 3
  },
  "AS Rajpoot": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "SC Ganguly": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 4
  },
  "RT Ponting": {
    "Bowler": "JH Kallis",
    "TimesDissmissed": 1
  },
  "DJ Hussey": {
    "Bowler": "SK Trivedi",
    "TimesDissmissed": 4
  },
  "Mohammad Hafeez": {
    "Bowler": "ST Jayasuriya",
    "TimesDissmissed": 2
  },
  "R Dravid": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "W Jaffer": {
    "Bowler": "AB Dinda",
    "TimesDissmissed": 1
  },
  "JH Kallis": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 4
  },
  "CL White": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 2
  },
  "MV Boucher": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 2
  },
  "B Akhil": {
    "Bowler": "JDP Oram",
    "TimesDissmissed": 2
  },
  "AA Noffke": {
    "Bowler": "SC Ganguly",
    "TimesDissmissed": 1
  },
  "SB Joshi": {
    "Bowler": "LR Shukla",
    "TimesDissmissed": 1
  },
  "ML Hayden": {
    "Bowler": "PJ Sangwan",
    "TimesDissmissed": 2
  },
  "MEK Hussey": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 4
  },
  "JDP Oram": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 2
  },
  "S Badrinath": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 3
  },
  "K Goel": {
    "Bowler": "MS Gony",
    "TimesDissmissed": 2
  },
  "JR Hopes": {
    "Bowler": "P Amarnath",
    "TimesDissmissed": 1
  },
  "KC Sangakkara": {
    "Bowler": "R Sharma",
    "TimesDissmissed": 5
  },
  "SM Katich": {
    "Bowler": "A Symonds",
    "TimesDissmissed": 2
  },
  "T Kohli": {
    "Bowler": "GD McGrath",
    "TimesDissmissed": 1
  },
  "M Kaif": {
    "Bowler": "S Sreesanth",
    "TimesDissmissed": 2
  },
  "DS Lehmann": {
    "Bowler": "MF Maharoof",
    "TimesDissmissed": 1
  },
  "M Rawat": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 1
  },
  "D Salunkhe": {
    "Bowler": "JR Hopes",
    "TimesDissmissed": 1
  },
  "SK Warne": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 3
  },
  "SK Trivedi": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 1
  },
  "V Sehwag": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 3
  },
  "L Ronchi": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "ST Jayasuriya": {
    "Bowler": "B Lee",
    "TimesDissmissed": 2
  },
  "DJ Thornely": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "PR Shah": {
    "Bowler": "B Akhil",
    "TimesDissmissed": 1
  },
  "AM Nayar": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 2
  },
  "SM Pollock": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "S Chanderpaul": {
    "Bowler": "DS Kulkarni",
    "TimesDissmissed": 1
  },
  "LRPL Taylor": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 2
  },
  "AC Gilchrist": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 4
  },
  "Y Venugopal Rao": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 3
  },
  "VVS Laxman": {
    "Bowler": "AB Dinda",
    "TimesDissmissed": 2
  },
  "A Symonds": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 3
  },
  "SB Styris": {
    "Bowler": "Mohammad Hafeez",
    "TimesDissmissed": 1
  },
  "AS Yadav": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 2
  },
  "SB Bangar": {
    "Bowler": "M Kartik",
    "TimesDissmissed": 1
  },
  "WPUJC Vaas": {
    "Bowler": "AB Agarkar",
    "TimesDissmissed": 1
  },
  "RP Singh": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 2
  },
  "LR Shukla": {
    "Bowler": "VRV Singh",
    "TimesDissmissed": 2
  },
  "DPMD Jayawardene": {
    "Bowler": "M Muralitharan",
    "TimesDissmissed": 3
  },
  "S Sohal": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 2
  },
  "B Lee": {
    "Bowler": "SK Trivedi",
    "TimesDissmissed": 1
  },
  "WA Mota": {
    "Bowler": "JA Morkel",
    "TimesDissmissed": 1
  },
  "Kamran Akmal": {
    "Bowler": "JR Hopes",
    "TimesDissmissed": 1
  },
  "Shahid Afridi": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 1
  },
  "DJ Bravo": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 3
  },
  "MA Khote": {
    "Bowler": "M Muralitharan",
    "TimesDissmissed": 1
  },
  "A Nehra": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "GC Smith": {
    "Bowler": "MF Maharoof",
    "TimesDissmissed": 2
  },
  "Pankaj Singh": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "RR Sarwan": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 1
  },
  "S Sreesanth": {
    "Bowler": "DS Kulkarni",
    "TimesDissmissed": 1
  },
  "VRV Singh": {
    "Bowler": "L Balaji",
    "TimesDissmissed": 1
  },
  "R Vinay Kumar": {
    "Bowler": "A Singh",
    "TimesDissmissed": 3
  },
  "AB Agarkar": {
    "Bowler": "MS Gony",
    "TimesDissmissed": 1
  },
  "M Kartik": {
    "Bowler": "JDP Oram",
    "TimesDissmissed": 1
  },
  "Shoaib Malik": {
    "Bowler": "VRV Singh",
    "TimesDissmissed": 1
  },
  "MF Maharoof": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "VY Mahesh": {
    "Bowler": "LR Shukla",
    "TimesDissmissed": 1
  },
  "TM Srivastava": {
    "Bowler": "VY Mahesh",
    "TimesDissmissed": 1
  },
  "B Chipli": {
    "Bowler": "MS Gony",
    "TimesDissmissed": 1
  },
  "DW Steyn": {
    "Bowler": "KA Pollard",
    "TimesDissmissed": 2
  },
  "DB Das": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 2
  },
  "HH Gibbs": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 3
  },
  "DNT Zoysa": {
    "Bowler": "S Sreesanth",
    "TimesDissmissed": 1
  },
  "D Kalyankrishna": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "SA Asnodkar": {
    "Bowler": "JA Morkel",
    "TimesDissmissed": 2
  },
  "Sohail Tanvir": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 1
  },
  "Salman Butt": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 1
  },
  "BJ Hodge": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 2
  },
  "Umar Gul": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 2
  },
  "SP Fleming": {
    "Bowler": "VY Mahesh",
    "TimesDissmissed": 1
  },
  "S Vidyut": {
    "Bowler": "GD McGrath",
    "TimesDissmissed": 1
  },
  "JA Morkel": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 2
  },
  "LPC Silva": {
    "Bowler": "V Kohli",
    "TimesDissmissed": 1
  },
  "DB Ravi Teja": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 3
  },
  "Misbah-ul-Haq": {
    "Bowler": "MF Maharoof",
    "TimesDissmissed": 2
  },
  "YV Takawale": {
    "Bowler": "VY Mahesh",
    "TimesDissmissed": 1
  },
  "RR Raje": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 1
  },
  "Mohammad Asif": {
    "Bowler": "DS Kulkarni",
    "TimesDissmissed": 1
  },
  "GD McGrath": {
    "Bowler": "DS Kulkarni",
    "TimesDissmissed": 1
  },
  "Joginder Sharma": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "MS Gony": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 1
  },
  "M Muralitharan": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 1
  },
  "M Ntini": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 1
  },
  "DT Patil": {
    "Bowler": "SK Warne",
    "TimesDissmissed": 1
  },
  "A Kumble": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 1
  },
  "S Anirudha": {
    "Bowler": "RP Singh",
    "TimesDissmissed": 1
  },
  "CK Kapugedera": {
    "Bowler": "VY Mahesh",
    "TimesDissmissed": 1
  },
  "A Chopra": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 1
  },
  "T Taibu": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 1
  },
  "J Arunkumar": {
    "Bowler": "Umar Gul",
    "TimesDissmissed": 1
  },
  "PP Ojha": {
    "Bowler": "SK Raina",
    "TimesDissmissed": 2
  },
  "SP Goswami": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 2
  },
  "SR Tendulkar": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 4
  },
  "TM Dilshan": {
    "Bowler": "I Sharma",
    "TimesDissmissed": 3
  },
  "AD Mascarenhas": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 2
  },
  "Niraj Patel": {
    "Bowler": "MF Maharoof",
    "TimesDissmissed": 1
  },
  "LA Pomersbach": {
    "Bowler": "PP Ojha",
    "TimesDissmissed": 1
  },
  "Younis Khan": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "Shoaib Akhtar": {
    "Bowler": "RR Raje",
    "TimesDissmissed": 1
  },
  "H Das": {
    "Bowler": "CRD Fernando",
    "TimesDissmissed": 1
  },
  "SD Chitnis": {
    "Bowler": "VRV Singh",
    "TimesDissmissed": 1
  },
  "CRD Fernando": {
    "Bowler": "VRV Singh",
    "TimesDissmissed": 1
  },
  "VS Yeligati": {
    "Bowler": "VRV Singh",
    "TimesDissmissed": 1
  },
  "L Balaji": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 1
  },
  "A Mukund": {
    "Bowler": "Sohail Tanvir",
    "TimesDissmissed": 1
  },
  "RR Powar": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 1
  },
  "JP Duminy": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 2
  },
  "A Flintoff": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 1
  },
  "T Thushara": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "JD Ryder": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 2
  },
  "KP Pietersen": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "T Henderson": {
    "Bowler": "JD Ryder",
    "TimesDissmissed": 1
  },
  "Kamran Khan": {
    "Bowler": "A Kumble",
    "TimesDissmissed": 1
  },
  "RS Bopara": {
    "Bowler": "Kamran Khan",
    "TimesDissmissed": 2
  },
  "R Bishnoi": {
    "Bowler": "M Muralitharan",
    "TimesDissmissed": 1
  },
  "FH Edwards": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "PC Valthaty": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 2
  },
  "RJ Quiney": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 2
  },
  "AS Raut": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 1
  },
  "Yashpal Singh": {
    "Bowler": "SK Warne",
    "TimesDissmissed": 1
  },
  "M Manhas": {
    "Bowler": "R Sharma",
    "TimesDissmissed": 2
  },
  "AA Bilakhia": {
    "Bowler": "DL Vettori",
    "TimesDissmissed": 1
  },
  "AN Ghosh": {
    "Bowler": "AM Nayar",
    "TimesDissmissed": 1
  },
  "BAW Mendis": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "DL Vettori": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "MN van Wyk": {
    "Bowler": "GR Napier",
    "TimesDissmissed": 1
  },
  "RE van der Merwe": {
    "Bowler": "I Sharma",
    "TimesDissmissed": 1
  },
  "TL Suman": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "Shoaib Ahmed": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 1
  },
  "GR Napier": {
    "Bowler": "CH Gayle",
    "TimesDissmissed": 1
  },
  "KP Appanna": {
    "Bowler": "RJ Harris",
    "TimesDissmissed": 1
  },
  "LA Carseldine": {
    "Bowler": "Y Venugopal Rao",
    "TimesDissmissed": 1
  },
  "M Vijay": {
    "Bowler": "S Aravind",
    "TimesDissmissed": 4
  },
  "SB Jakati": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 1
  },
  "RJ Harris": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 2
  },
  "D du Preez": {
    "Bowler": "A Singh",
    "TimesDissmissed": 1
  },
  "M Morkel": {
    "Bowler": "RG Sharma",
    "TimesDissmissed": 1
  },
  "J Botha": {
    "Bowler": "Anand Rajan",
    "TimesDissmissed": 2
  },
  "C Nanda": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "A Singh": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 1
  },
  "GJ Bailey": {
    "Bowler": "MC Henriques",
    "TimesDissmissed": 3
  },
  "AB McDonald": {
    "Bowler": "A Kumble",
    "TimesDissmissed": 1
  },
  "Y Nagar": {
    "Bowler": "B Lee",
    "TimesDissmissed": 2
  },
  "R Ashwin": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 3
  },
  "Mohammad Ashraful": {
    "Bowler": "PJ Sangwan",
    "TimesDissmissed": 1
  },
  "CA Pujara": {
    "Bowler": "JH Kallis",
    "TimesDissmissed": 2
  },
  "OA Shah": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 2
  },
  "Anirudh Singh": {
    "Bowler": "LR Shukla",
    "TimesDissmissed": 1
  },
  "Jaskaran Singh": {
    "Bowler": "S Sreesanth",
    "TimesDissmissed": 1
  },
  "R Sathish": {
    "Bowler": "JA Morkel",
    "TimesDissmissed": 2
  },
  "R McLaren": {
    "Bowler": "R Ashwin",
    "TimesDissmissed": 1
  },
  "AA Jhunjhunwala": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "P Dogra": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 3
  },
  "A Uniyal": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "MS Bisla": {
    "Bowler": "JA Morkel",
    "TimesDissmissed": 2
  },
  "JM Kemp": {
    "Bowler": "PP Ojha",
    "TimesDissmissed": 1
  },
  "RS Gavaskar": {
    "Bowler": "JM Kemp",
    "TimesDissmissed": 1
  },
  "SE Bond": {
    "Bowler": "R Ashwin",
    "TimesDissmissed": 1
  },
  "S Ladda": {
    "Bowler": "ST Jayasuriya",
    "TimesDissmissed": 1
  },
  "DP Nannes": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 1
  },
  "MJ Lumb": {
    "Bowler": "JA Morkel",
    "TimesDissmissed": 2
  },
  "DR Martyn": {
    "Bowler": "A Kumble",
    "TimesDissmissed": 1
  },
  "S Narwal": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 1
  },
  "AB Barath": {
    "Bowler": "PP Ojha",
    "TimesDissmissed": 1
  },
  "FY Fazal": {
    "Bowler": "SE Bond",
    "TimesDissmissed": 1
  },
  "AC Voges": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "MD Mishra": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 2
  },
  "J Theron": {
    "Bowler": "L Balaji",
    "TimesDissmissed": 1
  },
  "SJ Srivastava": {
    "Bowler": "SW Tait",
    "TimesDissmissed": 1
  },
  "R Sharma": {
    "Bowler": "S Narwal",
    "TimesDissmissed": 1
  },
  "SW Tait": {
    "Bowler": "V Sehwag",
    "TimesDissmissed": 1
  },
  "KB Arun Karthik": {
    "Bowler": "Iqbal Abdulla",
    "TimesDissmissed": 1
  },
  "KAJ Roach": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "PD Collingwood": {
    "Bowler": "CK Langeveldt",
    "TimesDissmissed": 1
  },
  "CK Langeveldt": {
    "Bowler": "AB McDonald",
    "TimesDissmissed": 1
  },
  "VS Malik": {
    "Bowler": "Z Khan",
    "TimesDissmissed": 1
  },
  "A Mithun": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 1
  },
  "AP Dole": {
    "Bowler": "RJ Harris",
    "TimesDissmissed": 1
  },
  "AN Ahmed": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 1
  },
  "DE Bollinger": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "S Sriram": {
    "Bowler": "AB Dinda",
    "TimesDissmissed": 1
  },
  "B Sumanth": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 1
  },
  "C Madan": {
    "Bowler": "AB McDonald",
    "TimesDissmissed": 1
  },
  "AG Paunikar": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 1
  },
  "MR Marsh": {
    "Bowler": "Yuvraj Singh",
    "TimesDissmissed": 1
  },
  "Harmeet Singh": {
    "Bowler": "M Muralitharan",
    "TimesDissmissed": 1
  },
  "RV Gomez": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 1
  },
  "AUK Pathan": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "UBT Chand": {
    "Bowler": "B Lee",
    "TimesDissmissed": 2
  },
  "DJ Jacobs": {
    "Bowler": "M Morkel",
    "TimesDissmissed": 1
  },
  "Sunny Singh": {
    "Bowler": "M Kartik",
    "TimesDissmissed": 1
  },
  "AL Menaria": {
    "Bowler": "YK Pathan",
    "TimesDissmissed": 2
  },
  "WD Parnell": {
    "Bowler": "SP Narine",
    "TimesDissmissed": 2
  },
  "JJ van der Wath": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 1
  },
  "R Ninan": {
    "Bowler": "DT Christian",
    "TimesDissmissed": 1
  },
  "MS Wade": {
    "Bowler": "R Sharma",
    "TimesDissmissed": 1
  },
  "TD Paine": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "SB Wagh": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "AC Thomas": {
    "Bowler": "MS Gony",
    "TimesDissmissed": 1
  },
  "JEC Franklin": {
    "Bowler": "SB Wagh",
    "TimesDissmissed": 1
  },
  "DH Yagnik": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 2
  },
  "S Randiv": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "BJ Haddin": {
    "Bowler": "J Syed Mohammad",
    "TimesDissmissed": 1
  },
  "NLTC Perera": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 2
  },
  "NL McCullum": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 2
  },
  "J Syed Mohammad": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 1
  },
  "RN ten Doeschate": {
    "Bowler": "M Morkel",
    "TimesDissmissed": 1
  },
  "TR Birt": {
    "Bowler": "YK Pathan",
    "TimesDissmissed": 1
  },
  "AG Murtaza": {
    "Bowler": "CH Gayle",
    "TimesDissmissed": 1
  },
  "Harpreet Singh": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 1
  },
  "M Klinger": {
    "Bowler": "M Morkel",
    "TimesDissmissed": 1
  },
  "AC Blizzard": {
    "Bowler": "AC Thomas",
    "TimesDissmissed": 1
  },
  "CA Ingram": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "CJ Ferguson": {
    "Bowler": "JP Duminy",
    "TimesDissmissed": 1
  },
  "AA Chavan": {
    "Bowler": "KA Pollard",
    "TimesDissmissed": 1
  },
  "ND Doshi": {
    "Bowler": "P Parameswaran",
    "TimesDissmissed": 1
  },
  "Y Gnaneswara Rao": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 1
  },
  "S Rana": {
    "Bowler": "L Balaji",
    "TimesDissmissed": 1
  },
  "RE Levi": {
    "Bowler": "DW Steyn",
    "TimesDissmissed": 2
  },
  "KK Cooper": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "HV Patel": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 2
  },
  "DJ Harris": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 1
  },
  "GB Hogg": {
    "Bowler": "MM Patel",
    "TimesDissmissed": 1
  },
  "RR Bhatkal": {
    "Bowler": "DE Bollinger",
    "TimesDissmissed": 2
  },
  "CJ McKay": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "N Saini": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 2
  },
  "Azhar Mahmood": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 2
  },
  "RJ Peterson": {
    "Bowler": "M Morkel",
    "TimesDissmissed": 1
  },
  "A Ashish Reddy": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 2
  },
  "V Pratap Singh": {
    "Bowler": "SL Malinga",
    "TimesDissmissed": 1
  },
  "BB Samantray": {
    "Bowler": "JP Faulkner",
    "TimesDissmissed": 2
  },
  "MJ Clarke": {
    "Bowler": "V Pratap Singh",
    "TimesDissmissed": 1
  },
  "Gurkeerat Singh": {
    "Bowler": "AD Russell",
    "TimesDissmissed": 3
  },
  "AP Majumdar": {
    "Bowler": "J Botha",
    "TimesDissmissed": 1
  },
  "PA Reddy": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 1
  },
  "P Awana": {
    "Bowler": "DP Nannes",
    "TimesDissmissed": 1
  },
  "AD Russell": {
    "Bowler": "P Parameswaran",
    "TimesDissmissed": 1
  },
  "A Chandila": {
    "Bowler": "V Pratap Singh",
    "TimesDissmissed": 1
  },
  "Sunny Gupta": {
    "Bowler": "SB Jakati",
    "TimesDissmissed": 1
  },
  "MC Juneja": {
    "Bowler": "L Balaji",
    "TimesDissmissed": 1
  },
  "GH Vihari": {
    "Bowler": "B Kumar",
    "TimesDissmissed": 2
  },
  "MDKJ Perera": {
    "Bowler": "UT Yadav",
    "TimesDissmissed": 1
  },
  "R Shukla": {
    "Bowler": "MA Starc",
    "TimesDissmissed": 1
  },
  "B Laughlin": {
    "Bowler": "KA Pollard",
    "TimesDissmissed": 1
  },
  "BMAJ Mendis": {
    "Bowler": "KA Pollard",
    "TimesDissmissed": 1
  },
  "R Rampaul": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 1
  },
  "SMSM Senanayake": {
    "Bowler": "P Kumar",
    "TimesDissmissed": 1
  },
  "BJ Rohrer": {
    "Bowler": "JD Unadkat",
    "TimesDissmissed": 1
  },
  "KL Rahul": {
    "Bowler": "DS Kulkarni",
    "TimesDissmissed": 3
  },
  "Q de Kock": {
    "Bowler": "AB Dinda",
    "TimesDissmissed": 2
  },
  "R Dhawan": {
    "Bowler": "STR Binny",
    "TimesDissmissed": 1
  },
  "LJ Wright": {
    "Bowler": "Azhar Mahmood",
    "TimesDissmissed": 1
  },
  "IC Pandey": {
    "Bowler": "CH Gayle",
    "TimesDissmissed": 1
  },
  "CM Gautam": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 1
  },
  "X Thalaivan Sargunam": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 1
  },
  "DJG Sammy": {
    "Bowler": "JP Faulkner",
    "TimesDissmissed": 2
  },
  "KW Richardson": {
    "Bowler": "CH Morris",
    "TimesDissmissed": 1
  },
  "UA Birla": {
    "Bowler": "Iqbal Abdulla",
    "TimesDissmissed": 1
  },
  "Parvez Rasool": {
    "Bowler": "L Balaji",
    "TimesDissmissed": 1
  },
  "PV Tambe": {
    "Bowler": "IK Pathan",
    "TimesDissmissed": 1
  },
  "NJ Maddinson": {
    "Bowler": "Mohammed Shami",
    "TimesDissmissed": 1
  },
  "JDS Neesham": {
    "Bowler": "PP Chawla",
    "TimesDissmissed": 1
  },
  "MA Starc": {
    "Bowler": "R Bhatia",
    "TimesDissmissed": 1
  },
  "BR Dunk": {
    "Bowler": "DJG Sammy",
    "TimesDissmissed": 1
  },
  "RR Rossouw": {
    "Bowler": "A Nehra",
    "TimesDissmissed": 2
  },
  "Shivam Sharma": {
    "Bowler": "MA Starc",
    "TimesDissmissed": 1
  },
  "VH Zol": {
    "Bowler": "R Tewatia",
    "TimesDissmissed": 1
  },
  "BE Hendricks": {
    "Bowler": "JJ Bumrah",
    "TimesDissmissed": 1
  },
  "S Gopal": {
    "Bowler": "Imran Tahir",
    "TimesDissmissed": 1
  },
  "M de Lange": {
    "Bowler": "Imran Tahir",
    "TimesDissmissed": 1
  },
  "JO Holder": {
    "Bowler": "R Vinay Kumar",
    "TimesDissmissed": 1
  },
  "Karanveer Singh": {
    "Bowler": "MA Starc",
    "TimesDissmissed": 1
  },
  "SA Abbott": {
    "Bowler": "TA Boult",
    "TimesDissmissed": 1
  },
  "J Suchith": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 1
  },
  "D Wiese": {
    "Bowler": "DJ Bravo",
    "TimesDissmissed": 2
  },
  "SN Khan": {
    "Bowler": "RA Jadeja",
    "TimesDissmissed": 1
  },
  "DJ Muthuswami": {
    "Bowler": "D Wiese",
    "TimesDissmissed": 1
  },
  "C Munro": {
    "Bowler": "DJ Hooda",
    "TimesDissmissed": 1
  },
  "KJ Abbott": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 1
  },
  "M Ashwin": {
    "Bowler": "KW Richardson",
    "TimesDissmissed": 1
  },
  "NS Naik": {
    "Bowler": "Mustafizur Rahman",
    "TimesDissmissed": 1
  },
  "PSP Handscomb": {
    "Bowler": "Harbhajan Singh",
    "TimesDissmissed": 1
  },
  "UT Khawaja": {
    "Bowler": "A Mishra",
    "TimesDissmissed": 1
  },
  "F Behardien": {
    "Bowler": "YS Chahal",
    "TimesDissmissed": 1
  },
  "S Kaushik": {
    "Bowler": "Sachin Baby",
    "TimesDissmissed": 1
  },
  "ER Dwivedi": {
    "Bowler": "SR Watson",
    "TimesDissmissed": 1
  }
}